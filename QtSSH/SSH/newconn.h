#ifndef NEWCONN_H
#define NEWCONN_H

#include <QMainWindow>
#include "common.h"

namespace Ui {
class NewConn;
}

class NewConn : public QMainWindow
{
    Q_OBJECT

public:
    explicit NewConn(QWidget *parent = nullptr);
    ~NewConn();

    void write(QString text);

    void readTcp();



private slots:
    void on_btn_close_clicked();

    void on_btn_confirm_clicked();

    void whileconnected();

    void whiledisconnected();

    void displayError(QAbstractSocket::SocketError);
private:
    Ui::NewConn *ui;
    QTcpSocket * Sock;
    quint16 blockSize;
    QString message;
    bool connected;
};



#endif // NEWCONN_H
