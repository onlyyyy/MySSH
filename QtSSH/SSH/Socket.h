#ifndef SOCKET_H
#define SOCKET_H
#include "common.h"

class MySocket{
public:
    void connect();

    void write(QString text);

    QString read();

private:
    QTcpSocket * socket;


};


#endif // SOCKET_H
