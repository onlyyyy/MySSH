#include "newconn.h"
#include "ui_newconn.h"

NewConn::NewConn(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::NewConn)
{
    ui->setupUi(this);
    setFixedSize(216,240);
    Sock = new QTcpSocket(this);
    connected = false;
    Sock->abort();
    Sock->connectToHost("127.0.0.1",13000,QTcpSocket::ReadWrite);
    connect(Sock,SIGNAL(connected()),this,SLOT(whileconnected()));
    connect(Sock,SIGNAL(disconnected()),this,SLOT(whiledisconnected()));
    connect(Sock,&QTcpSocket::readyRead,this,&NewConn::readTcp);
    connect(Sock,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));
}

NewConn::~NewConn()
{
    delete ui;
}

void NewConn::on_btn_close_clicked()
{
    this->close();
}

void NewConn::on_btn_confirm_clicked()
{
    if(connected == false)
    {
        QMessageBox::warning(nullptr,"警告","连接服务器失败!");
    }
    QJsonObject conn;
    DJY::EasyQJson easy;
    QString Ip = ui->lineEdit_ip->text();
    QString port = ui->lineEdit_port->text();
    QString user = ui->lineEdit_user->text();
    QString passwd = ui->lineEdit_password->text();
    QString name = ui->lineEdit_name->text();
    conn["type"] = 1000;
    conn["name"] = name;
    conn["Ip"] = Ip;
    conn["Port"] = port;
    conn["User"] = user;
    conn["Passwd"] = passwd;

    QString ask = easy.readObjectReturnQString(conn);
    qDebug()<<"发给服务端的数据为"<<ask;


    write(ask);

    this->close();
}







void NewConn::write(QString text)
{
    Sock->write(text.toLocal8Bit());
}


void NewConn::readTcp()
{
    qDebug()<<"接收到数据";
    QDataStream in(Sock);//数据流入套接字
    in.setVersion(QDataStream::Qt_5_5);
    //如果是刚开始接收数据，因为初始化大小为0
    if(blockSize==0)
    {
        //判断所接收的数据是否大于两个字节，也就是文件的大小信息的空间
        //如果是，则把文件大小信息保存到blocksize
        if(Sock->bytesAvailable()<static_cast<int>(sizeof(quint16)))
            return;
        in>>blockSize;//发送和接收的数据流都是有自己的格式的，比如，自己定义的quint16 blocksize;

    }
    //如果没有得到全部的数据，则返回，继续接收数据
    if(Sock->bytesAvailable()<blockSize)
        return;
    in>>message;
    if(message != "success")
    {
        QMessageBox::warning(nullptr,"警告","新建连接失败!");
    }
}

void NewConn::displayError(QAbstractSocket::SocketError)
{
    qDebug()<<"出错";
    QString error = Sock->errorString();
    QMessageBox::warning(nullptr,"警告",error);
}

void NewConn::whileconnected()
{
    connected = true;
}

void NewConn::whiledisconnected()
{
    connected = false;
}
