#include "myssh.h"
#include "ui_myssh.h"

MySSH::MySSH(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MySSH)
{
    ui->setupUi(this);
    connected = false;
    tcpSocket = new QTcpSocket;
    ui->textEdit_send->setPlaceholderText("输入要发送的内容");
    connect(tcpSocket,&QTcpSocket::readyRead,this,&MySSH::receiveSocket);
    tcpSocket->abort();
    tcpSocket->connectToHost("127.0.0.1",13000,QTcpSocket::ReadWrite);
    connect(tcpSocket,SIGNAL(connected()),this,SLOT(whileconnected()));
    connect(tcpSocket,SIGNAL(disconnected()),this,SLOT(whiledisconnected()));
    connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));

}

MySSH::~MySSH()
{
    delete ui;
}


void MySSH::on_btn_close_clicked()
{
    close();
}

void MySSH::on_btn_add_clicked()
{
    newConn = new NewConn;
    newConn->show();
}

void MySSH::tcpConnect()
{

    tcpSocket->connectToHost("127.0.0.1",13000,QTcpSocket::ReadWrite);

}

void MySSH::tcpWrite(QString text)
{
    tcpSocket->write(text.toLocal8Bit());
}

void MySSH::receiveSocket()
{
    qDebug()<<"接收到数据";
//    QDataStream in(tcpSocket);//数据流入套接字
//    in.setVersion(QDataStream::Qt_5_5);
//    //如果是刚开始接收数据，因为初始化大小为0
//    if(blockSize==0)
//    {
//        //判断所接收的数据是否大于两个字节，也就是文件的大小信息的空间
//        //如果是，则把文件大小信息保存到blocksize
//        if(tcpSocket->bytesAvailable()<static_cast<int>(sizeof(quint16)))
//            return;
//        in>>blockSize;//发送和接收的数据流都是有自己的格式的，比如，自己定义的quint16 blocksize;

//    }
//    //如果没有得到全部的数据，则返回，继续接收数据
//    if(tcpSocket->bytesAvailable()<blockSize)
//        return;
//    in>>message;
//    qDebug()<<"接收到数据"<<message;
    QDateTime now = QDateTime::currentDateTime();
    QString nowTime = now.toString("yyyy-MM-dd hh:mm:ss");
    QByteArray array = tcpSocket->readAll();
    QString result = array;

    ui->textEdit_receive->append(nowTime + " received "+result);
}

void MySSH::displayError(QAbstractSocket::SocketError)
{
    QString error = tcpSocket->errorString();
    QMessageBox::warning(nullptr,"警告",error);
}

void MySSH::on_btn_send_clicked()
{
    if(connected == false)
    {
        QMessageBox::warning(nullptr,"警告","服务器连接出错");
        return;
    }
    DJY::EasyQJson easy;
    QJsonObject ob;
    QString sendValue = ui->textEdit_send->toPlainText();
    ob["type"] = 1001;
    ob["value"] = sendValue;
    QString send = easy.readObjectReturnQString(ob);
    QDateTime now = QDateTime::currentDateTime();
    QString nowTime = now.toString("yyyy-MM-dd hh:mm:ss");
    ui->textEdit_receive->append(nowTime + " send: "+sendValue);
    ui->textEdit_send->clear();
    qDebug()<<"发给服务器的数据为"<<send;
    ui->textEdit_receive->repaint();
    ui->textEdit_send->repaint();
    QByteArray arr = send.toLocal8Bit();
    tcpSocket->write(arr);


}

void MySSH::whileconnected()
{
    connected = true;
}

void MySSH::whiledisconnected()
{
    connected = false;
}

void MySSH::on_btn_reconn_clicked()
{
    DJY::EasyQJson easy;
    connected = false;
    tcpSocket = new QTcpSocket;
    tcpSocket->connectToHost("127.0.0.1",13000,QTcpSocket::ReadWrite);
    QJsonObject ob;
    ob["type"] = 1001;
    QString send = easy.readObjectReturnQString(ob);
    QByteArray arr = send.toLocal8Bit();
    tcpSocket->write(arr);
}
