#ifndef MYSSH_H
#define MYSSH_H
#include "common.h"
#include <QMainWindow>
#include "newconn.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MySSH; }
QT_END_NAMESPACE

class MySSH : public QMainWindow
{
    Q_OBJECT

public:
    MySSH(QWidget *parent = nullptr);
    ~MySSH();

    void tcpConnect();
    void tcpWrite(QString text);

private slots:
    void on_btn_close_clicked();

    void on_btn_add_clicked();

    void receiveSocket();
    void whileconnected();

    void whiledisconnected();
    void displayError(QAbstractSocket::SocketError);

    void on_btn_send_clicked();

    void on_btn_reconn_clicked();

private:
    Ui::MySSH *ui;
    NewConn *newConn;
    QString filePath;
    QTcpSocket * tcpSocket;
    quint16 blockSize;
    QString message;
    bool connected;

};
#endif // MYSSH_H
