/********************************************************************************
** Form generated from reading UI file 'newconn.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWCONN_H
#define UI_NEWCONN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NewConn
{
public:
    QWidget *MainLayput;
    QFormLayout *formLayout_2;
    QFormLayout *mainFormLayout;
    QHBoxLayout *ipLayout;
    QLabel *label_ip;
    QLineEdit *lineEdit_ip;
    QHBoxLayout *protLayout;
    QLabel *label_prot;
    QComboBox *comboBox_SSHType;
    QHBoxLayout *portLayout;
    QLabel *label_port;
    QLineEdit *lineEdit_port;
    QHBoxLayout *userLayout;
    QLabel *label_user;
    QLineEdit *lineEdit_user;
    QHBoxLayout *passwdLayout;
    QLabel *label_password;
    QLineEdit *lineEdit_password;
    QHBoxLayout *horizontalLayout;
    QPushButton *btn_confirm;
    QPushButton *btn_close;
    QMenuBar *menubar;

    void setupUi(QMainWindow *NewConn)
    {
        if (NewConn->objectName().isEmpty())
            NewConn->setObjectName(QString::fromUtf8("NewConn"));
        NewConn->resize(216, 256);
        MainLayput = new QWidget(NewConn);
        MainLayput->setObjectName(QString::fromUtf8("MainLayput"));
        formLayout_2 = new QFormLayout(MainLayput);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        mainFormLayout = new QFormLayout();
        mainFormLayout->setObjectName(QString::fromUtf8("mainFormLayout"));
        ipLayout = new QHBoxLayout();
        ipLayout->setObjectName(QString::fromUtf8("ipLayout"));
        label_ip = new QLabel(MainLayput);
        label_ip->setObjectName(QString::fromUtf8("label_ip"));

        ipLayout->addWidget(label_ip);

        lineEdit_ip = new QLineEdit(MainLayput);
        lineEdit_ip->setObjectName(QString::fromUtf8("lineEdit_ip"));

        ipLayout->addWidget(lineEdit_ip);


        mainFormLayout->setLayout(0, QFormLayout::FieldRole, ipLayout);

        protLayout = new QHBoxLayout();
        protLayout->setObjectName(QString::fromUtf8("protLayout"));
        label_prot = new QLabel(MainLayput);
        label_prot->setObjectName(QString::fromUtf8("label_prot"));

        protLayout->addWidget(label_prot);

        comboBox_SSHType = new QComboBox(MainLayput);
        comboBox_SSHType->addItem(QString());
        comboBox_SSHType->addItem(QString());
        comboBox_SSHType->setObjectName(QString::fromUtf8("comboBox_SSHType"));

        protLayout->addWidget(comboBox_SSHType);


        mainFormLayout->setLayout(1, QFormLayout::SpanningRole, protLayout);

        portLayout = new QHBoxLayout();
        portLayout->setObjectName(QString::fromUtf8("portLayout"));
        label_port = new QLabel(MainLayput);
        label_port->setObjectName(QString::fromUtf8("label_port"));

        portLayout->addWidget(label_port);

        lineEdit_port = new QLineEdit(MainLayput);
        lineEdit_port->setObjectName(QString::fromUtf8("lineEdit_port"));

        portLayout->addWidget(lineEdit_port);


        mainFormLayout->setLayout(2, QFormLayout::FieldRole, portLayout);

        userLayout = new QHBoxLayout();
        userLayout->setObjectName(QString::fromUtf8("userLayout"));
        label_user = new QLabel(MainLayput);
        label_user->setObjectName(QString::fromUtf8("label_user"));

        userLayout->addWidget(label_user);

        lineEdit_user = new QLineEdit(MainLayput);
        lineEdit_user->setObjectName(QString::fromUtf8("lineEdit_user"));

        userLayout->addWidget(lineEdit_user);


        mainFormLayout->setLayout(3, QFormLayout::FieldRole, userLayout);

        passwdLayout = new QHBoxLayout();
        passwdLayout->setObjectName(QString::fromUtf8("passwdLayout"));
        label_password = new QLabel(MainLayput);
        label_password->setObjectName(QString::fromUtf8("label_password"));

        passwdLayout->addWidget(label_password);

        lineEdit_password = new QLineEdit(MainLayput);
        lineEdit_password->setObjectName(QString::fromUtf8("lineEdit_password"));

        passwdLayout->addWidget(lineEdit_password);


        mainFormLayout->setLayout(4, QFormLayout::FieldRole, passwdLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btn_confirm = new QPushButton(MainLayput);
        btn_confirm->setObjectName(QString::fromUtf8("btn_confirm"));

        horizontalLayout->addWidget(btn_confirm);

        btn_close = new QPushButton(MainLayput);
        btn_close->setObjectName(QString::fromUtf8("btn_close"));

        horizontalLayout->addWidget(btn_close);


        mainFormLayout->setLayout(5, QFormLayout::FieldRole, horizontalLayout);


        formLayout_2->setLayout(0, QFormLayout::LabelRole, mainFormLayout);

        NewConn->setCentralWidget(MainLayput);
        menubar = new QMenuBar(NewConn);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 216, 22));
        NewConn->setMenuBar(menubar);

        retranslateUi(NewConn);

        QMetaObject::connectSlotsByName(NewConn);
    } // setupUi

    void retranslateUi(QMainWindow *NewConn)
    {
        NewConn->setWindowTitle(QApplication::translate("NewConn", "MainWindow", nullptr));
        label_ip->setText(QApplication::translate("NewConn", "IP", nullptr));
        label_prot->setText(QApplication::translate("NewConn", "\345\215\217\350\256\256\347\261\273\345\236\213", nullptr));
        comboBox_SSHType->setItemText(0, QApplication::translate("NewConn", "SSH2", nullptr));
        comboBox_SSHType->setItemText(1, QApplication::translate("NewConn", "SSH", nullptr));

        label_port->setText(QApplication::translate("NewConn", "\347\253\257\345\217\243", nullptr));
        label_user->setText(QApplication::translate("NewConn", "\347\224\250\346\210\267\345\220\215", nullptr));
        label_password->setText(QApplication::translate("NewConn", "\345\257\206\347\240\201", nullptr));
        btn_confirm->setText(QApplication::translate("NewConn", "\347\241\256\350\256\244", nullptr));
        btn_close->setText(QApplication::translate("NewConn", "\350\277\224\345\233\236", nullptr));
    } // retranslateUi

};

namespace Ui {
    class NewConn: public Ui_NewConn {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWCONN_H
