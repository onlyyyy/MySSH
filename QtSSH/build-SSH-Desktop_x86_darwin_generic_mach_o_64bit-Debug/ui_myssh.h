/********************************************************************************
** Form generated from reading UI file 'myssh.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYSSH_H
#define UI_MYSSH_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MySSH
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QListView *listView_conn;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *textLayout;
    QTextEdit *textEdit_receive;
    QTextEdit *textEdit_send;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *CLoseSpacer;
    QPushButton *btn_close;
    QHBoxLayout *btnLayout;
    QPushButton *btn_add;
    QPushButton *btn_del;
    QMenuBar *menubar;

    void setupUi(QMainWindow *MySSH)
    {
        if (MySSH->objectName().isEmpty())
            MySSH->setObjectName(QString::fromUtf8("MySSH"));
        MySSH->resize(800, 600);
        centralwidget = new QWidget(MySSH);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        listView_conn = new QListView(centralwidget);
        listView_conn->setObjectName(QString::fromUtf8("listView_conn"));
        listView_conn->setMaximumSize(QSize(150, 16777215));

        gridLayout->addWidget(listView_conn, 0, 0, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        textLayout = new QVBoxLayout();
        textLayout->setObjectName(QString::fromUtf8("textLayout"));
        textEdit_receive = new QTextEdit(centralwidget);
        textEdit_receive->setObjectName(QString::fromUtf8("textEdit_receive"));
        textEdit_receive->setReadOnly(true);

        textLayout->addWidget(textEdit_receive);

        textEdit_send = new QTextEdit(centralwidget);
        textEdit_send->setObjectName(QString::fromUtf8("textEdit_send"));
        textEdit_send->setMaximumSize(QSize(16777215, 50));

        textLayout->addWidget(textEdit_send);


        verticalLayout_2->addLayout(textLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        CLoseSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(CLoseSpacer);

        btn_close = new QPushButton(centralwidget);
        btn_close->setObjectName(QString::fromUtf8("btn_close"));

        horizontalLayout->addWidget(btn_close);


        verticalLayout_2->addLayout(horizontalLayout);


        gridLayout->addLayout(verticalLayout_2, 0, 1, 2, 1);

        btnLayout = new QHBoxLayout();
        btnLayout->setObjectName(QString::fromUtf8("btnLayout"));
        btn_add = new QPushButton(centralwidget);
        btn_add->setObjectName(QString::fromUtf8("btn_add"));
        btn_add->setMaximumSize(QSize(75, 16777215));

        btnLayout->addWidget(btn_add);

        btn_del = new QPushButton(centralwidget);
        btn_del->setObjectName(QString::fromUtf8("btn_del"));
        btn_del->setMaximumSize(QSize(75, 16777215));

        btnLayout->addWidget(btn_del);


        gridLayout->addLayout(btnLayout, 1, 0, 1, 1);

        MySSH->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MySSH);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 22));
        MySSH->setMenuBar(menubar);

        retranslateUi(MySSH);

        QMetaObject::connectSlotsByName(MySSH);
    } // setupUi

    void retranslateUi(QMainWindow *MySSH)
    {
        MySSH->setWindowTitle(QApplication::translate("MySSH", "MySSH", nullptr));
#ifndef QT_NO_ACCESSIBILITY
        textEdit_send->setAccessibleDescription(QString());
#endif // QT_NO_ACCESSIBILITY
        btn_close->setText(QApplication::translate("MySSH", "\351\200\200\345\207\272", nullptr));
        btn_add->setText(QApplication::translate("MySSH", "\345\242\236\345\212\240", nullptr));
        btn_del->setText(QApplication::translate("MySSH", "\345\210\240\351\231\244", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MySSH: public Ui_MySSH {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYSSH_H
