const net = require('net')

const port = 13000;
const hostname = '127.0.0.1';
let SSH = require('../feature/SSH');
class TcpServer {
    constructor() {


    }

    async mainFunc() {
        this.server = new net.createServer();
        this.server.on('connection', async(client) => {

            console.log("收到一个连接 ");
            client.on('data',async function (msg) { //接收client发来的信息

                
                let resValue = JSON.parse(msg)
               // resValue = iconv.decode(msg,'utf8');
                console.log(`客户端发来一个信息：${resValue}`);
                console.log("resValue.type = ",resValue.type);
                let type = parseInt(resValue.type);       //与客户端预先定义接口

                switch (type) {
                    case 1000:
                        //读写Json创建新的连接
                        break;
                    case 1001:
                        let ssh = new SSH();
                        
                        let result = await ssh.use(resValue.value);
                        result = result.toString();
                        console.log("result = ",result);
                        client.write(result);   
                    default:
                        break;
                }
                

                
            });

            client.on('error', function (e) { //监听客户端异常
                console.log('client error' + e);
                client.end();
            });

            client.on('close', function () {

                console.log(`客户端下线了`);
            });

        });
        this.server.listen( port,hostname,function () {
            console.log(`服务器运行在：http://${hostname}:${port}`);
          });

    }


}

module.exports = TcpServer;